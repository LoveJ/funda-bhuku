import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Nav, NavItem, Navbar, Button } from 'react-bootstrap';

import SignOutButton from '../SignOut';
import * as routes from '../../constants/routes';

const Navigation = (props, { authUser }) =>

  <div>
    { authUser
        ? <NavigationAuth />
        : <NavigationNonAuth />
    }
  </div>

Navigation.contextTypes = {
  authUser: PropTypes.object,
};

const NavigationAuth = () =>
<Navbar inverse collapseOnSelect>
  <Navbar.Header>
    <Navbar.Brand>
      <a href="/"> lets Chat </a>
    </Navbar.Brand>
     <Navbar.Toggle />
  </Navbar.Header>
  <Navbar.Collapse>
    <Nav>
      <NavItem eventKey={1}>
        <Link to={routes.HOME}>Home</Link>
      </NavItem>
      <NavItem eventKey={2}>
        <Link to={routes.ACCOUNT}>Account</Link>
      </NavItem>

    </Nav>
    <Nav pullRight>
      <NavItem eventKey={3} href="#">
        <SignOutButton />
      </NavItem>
    </Nav>
  </Navbar.Collapse>
</Navbar>

const NavigationNonAuth = () =>
  <Navbar inverse collapseOnSelect>
  <Navbar.Header>
    <Navbar.Brand>
      <a href="/home"> lets Chat </a>
    </Navbar.Brand>
     <Navbar.Toggle />
  </Navbar.Header>
  <Navbar.Collapse>
    <Nav>
      <NavItem eventKey={4}>
        <Link to={routes.LANDING}>Landing</Link>
      </NavItem>
    </Nav>
    <Nav pullRight>
      <NavItem eventKey={5}>
        <Link to={routes.SIGN_IN}><Button> Sign In</Button> </Link>
      </NavItem>
    </Nav>
  </Navbar.Collapse>
</Navbar>

export default Navigation;
