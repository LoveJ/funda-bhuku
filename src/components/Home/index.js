import React, { Component } from 'react';
import Particles from 'react-particles-js';
import PropTypes from 'prop-types';
import { Grid, Row, Col, Jumbotron, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import withAuthorization from '../Session/withAuthorization';
import { db } from '../../firebase';


function roomIdGenerator() {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < 6; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
    return "/room/"+text;
}



class HomePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: {}
    };
  }

  componentDidMount() {
    db.onceGetUsers().then(snapshot =>
      this.setState(() => ({ users: snapshot.val() }))
    );

  }

  render() {
    const { users } = this.state;

    return (
      <div>
        <Particles
          params={{
                    particles: {
                      line_linked: {
                        shadow: {
                          enable: true,
                          color: "tomato",
                          blur: 5
                        },
                        "distance": 200,
                      }
                    }
                  }}
                  style={
                    {width: '100%', position: "absolute"}
                  }
          />

      <hr/>

        <Grid>
        <p>Welcome to our private messaging app.</p>
          <Row className="show-grid">
            <Col xs={6} md={4}>
              { !!users && <UserList users={users} /> }
            </Col>
            <Col xs={12} md={8}>
              <Jumbotron align="middle" className="bsStyle">
                <h1> &lt;lets Chat/&gt;</h1><br/>
                <p>you can start a random chat room from here.</p><br/><br/>
                <Link to={roomIdGenerator()}><Button bsStyle="default" bsSize="large">start Chat room...</Button></Link>
              </Jumbotron>
            </Col>

          </Row>
        </Grid>
      </div>
    );
  }
}


const UserList = ({ users }, {authUser}) =>

  <div>
    <p>(logged in as {authUser.email})</p>
    <p>(Available users)</p>
    {Object.keys(users).map(key =>
      <div key={key}><Link to={`chat/${authUser.uid}+${key}`}>{users[key].username}</Link></div>
    )}
  </div>

UserList.contextTypes = {
  authUser: PropTypes.object,
};

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(HomePage);