import React from 'react';
import { PageHeader, Grid, Row, Col, Image } from 'react-bootstrap';
import Particles from 'react-particles-js';
// import left_front from '../../images/left_front.png';

const LandingPage = () =>
  <div>
    <Particles
      params={{
                particles: {
                  line_linked: {
                    shadow: {
                      enable: true,
                      color: "tomato",
                      blur: 5
                    },
                    "distance": 200,
                  }
                }
              }}
              style={
                {width: '100%', position: "absolute"}
              }
      />

      <hr/>


    <PageHeader>
      Welcome to lets Chat <small>lets connect u</small>
    </PageHeader>
    <Grid>
      <Row>
        <Col xs={8} md={4} sm={4}  lg={4}>
          <Image src="images/left_front.png" rounded />
        </Col>
        <Col xs={8} md={4} sm={4}  lg={4}>
          <Image src="images/center_front.png" rounded />
        </Col>
        <Col xs={8} md={4} sm={4}  lg={4}>
          <Image src="images/right_front.png" rounded />
        </Col>
      </Row>
    </Grid>

  <span>Made with love by <a href="https://lovemorejokonya.com">Lovemore</a></span> | <span>Star the <a href="https://gitlab.com/lovej/funda-bhuku">Repository</a></span> | <span>Receive an <a href="http://www.lovejapps.com">update when we go live</a></span>
  </div>


export default LandingPage;
