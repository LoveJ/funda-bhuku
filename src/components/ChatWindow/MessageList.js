import React, {Component} from 'react';
import Message from './Message';

class MessageList extends Component {
  render() {

const message = this.props.messages.map(message => {
    return (<Message
      key = {message.id}
      username = {message.username}
      content = {message.content}
      type = {message.type} />)
});

    return (
            <main className="messages">
              {message}
            </main>

    );
  }
}
export default MessageList;
console.log("Rendering <MessageList/>");