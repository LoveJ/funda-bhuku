import React, {Component} from 'react';
import PropTypes from 'prop-types';
import{ FormControl} from 'react-bootstrap';

class Chatbar extends Component {

  constructor(props, {authUser}){
    super(props)
      this.state = {
      currentUser: {name: authUser.email}
    }
  }

  render() {
    const name = this.state.currentUser.name
    // const room = this.props.currentRoom
    return (
      <footer className="chatbar">



          <FormControl className="chatbar-username" placeholder="Your name HERE!" defaultValue = {name}/>



          <FormControl className="chatbar-message" placeholder="Type a message and hit ENTER"  onKeyDown= {this.props.sendMessage} />



      </footer>
    );
  }
}

Chatbar.contextTypes = {
  authUser: PropTypes.object,
};

export default Chatbar;
console.log("Rendering <Chatbar/>");
