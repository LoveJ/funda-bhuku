import React, { Component } from 'react';
import Particles from 'react-particles-js';
import { Grid, Row, Col } from 'react-bootstrap';
import PropTypes from 'prop-types';
import Chatbar from './Chatbar';
import MessageList from './MessageList';
import Video from './Video';
import withAuthorization from '../Session/withAuthorization';

const PORT = process.env.PORT || 3001;  // comment this line when deploying to heroku

class ChatWindow extends Component {
// const ChatWindow = (props, { authUser }) =>

// set initial state
  constructor(props, {authUser}) {
  super(props);
  console.log(`this.window.location.pathname:${window.location.pathname}`);
  this.state = {
    // messageFromNodeHttpServer: "Loading...",
    currentUser: {name: authUser.email}, // optional. if currentUser is not defined, it means the user is Anonymous
    messages: [ ],
    // new_user: "",
    onlineUsers: "1",
    room: `${window.location.pathname}`
  }; //end of set initial state Object

  this.sendMessage = this.sendMessage.bind(this)
  this.addMessage = this.addMessage.bind(this)
  // this.reverseUrl = this.reverseUrl.bind(this)
}


async componentDidMount() {
    console.log("componentDidMount <ChatWindow />");
    const url = `ws://${window.location.hostname}:${PORT}`; // for localhost only. comment when deploying
    // const url = `wss://${window.location.hostname}:${window.location.port}`; //for deployment only. uncomment when deploying
    console.log(`url:${url}`);

    this.socket = new WebSocket(url);
    const initialMsg ={
      room: window.location.pathname,
      type: "initialMsg",
      // client_from_id: this.state.currentUser.name
    }
    this.socket.onopen = (event) => {
      console.log("Connected to server");
      this.socket.send(JSON.stringify(initialMsg));
    }
    this.socket.onmessage = this.addMessage;
    // new code
    try {
      const data = await fetch('/example-path');
      const json = await data.json();
      this.setState(json);
    } catch (e) {
      console.log("Failed to fetch message", e);
    }
  }

//function to send message
sendMessage (event) {
  if(event.key === "Enter"){

    let username = this.state.currentUser.name;
    let content = event.target.value;

    let newMessageAsObject = {
      username: username,
      content: content,
      type: "postMessage",
      room: window.location.pathname
    };

  console.log(newMessageAsObject);
  this.socket.send(JSON.stringify(newMessageAsObject));
  event.target.value="";
  }
}

addMessage (received_message) {
  // This gets called when we receive a broadcasted message from the server.
  // We parse the JSON and add it to the message window.
  let new_message = JSON.parse(received_message.data);
  // if a new user connects, update status for onlineUsers
  if (new_message.type === "onlineStatus"){
    this.setState({onlineUsers: new_message.content});
    console.log(this.state.onlineUsers);
    // let statusValue = this.state.onlineUsers;
  } else {
    // debugger;
    console.log("new message: ", new_message);
    this.state.messages.push(new_message);
    this.setState({messages: this.state.messages});
  }
}



render(){

  //function to reverse url
const reverseUrl = (str) => {
  if (str){
    return "/room/"+str.substring(6).split('+').reverse().join('+')
  }
}

console.log(`reverseUrl(this.state.room):${reverseUrl(this.state.room)}`);

  return (
      <div>
        <Particles
          params={{
                    particles: {
                      line_linked: {
                        shadow: {
                          enable: true,
                          color: "tomato",
                          blur: 5
                        },
                        "distance": 200,
                      }
                    }
                  }}
                  style={
                    {width: '100%', position: "absolute"}
                  }
          />

      <hr/>


        <h1>ChatRoom window: {this.state.messageFromNodeHttpServer }</h1>
        <Grid>
          <Row className="show-grid">
            <Col xs={4} sm={3} md={3}  lg={2}>
              <Video room = {this.state.room} />
            </Col>
            <Col xs={12} sm={14} md={9} lg={10}>
              <MessageList messages={ this.state.messages } />
            </Col>
          </Row>
        </Grid>
        <Chatbar currentUser={ this.state.currentUser.name} currentRoom={this.state.room} sendMessage={this.sendMessage} />
      </div>
    );
  }
}

ChatWindow.contextTypes = {
  authUser: PropTypes.object,
};

const authCondition = (authUser) => !!authUser;

export default withAuthorization(authCondition)(ChatWindow);