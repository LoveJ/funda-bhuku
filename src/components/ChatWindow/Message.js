import React, {Component} from 'react';

class Message extends Component {
  render() {
    // if message is of type postMessage display normal
     if (this.props.type === "postMessage"){
      return (
          <div className="message">
            <span className="message-username"> {this.props.username} </span>
            <span className="message-content">{this.props.content}</span>
          </div> );
    } else if (this.props.type === "notification"){ // if its a notification use different format
      return (
          <div className="message system">
            {this.props.content}
          </div> );
    }
  }
}
export default Message;
console.log("Rendering <Message/>");
