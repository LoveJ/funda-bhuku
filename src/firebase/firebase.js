import * as firebase from 'firebase';

const devConfig = {
    apiKey: "AIzaSyB2YALZdUG9PD2uGSd5K5vFDrnjXmmkdd4",
    authDomain: "chatty-app-lj.firebaseapp.com",
    databaseURL: "https://chatty-app-lj.firebaseio.com",
    projectId: "chatty-app-lj",
    storageBucket: "chatty-app-lj.appspot.com",
    messagingSenderId: "844498951570"
  };

const prodConfig = {
  apiKey: "AIzaSyAy5T77jxRh8-ny7f2W8jUpzjdWo-5Hrk8",
  authDomain: "chatty-app-lj-prod.firebaseapp.com",
  databaseURL: "https://chatty-app-lj-prod.firebaseio.com",
  projectId: "chatty-app-lj-prod",
  storageBucket: "chatty-app-lj-prod.appspot.com",
  messagingSenderId: "816342980671"
};

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();
const auth = firebase.auth();

export {
  db,
  auth,
};