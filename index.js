const express = require('express');
const helmet = require('helmet');
const expressEnforcesSSL = require('express-enforces-ssl');
const WebSocket = require('ws').Server;
const http = require('http');
const uuid = require('node-uuid');
const PORT = process.env.PORT || 3001;
const ENV = process.env.ENV || "development";
const bodyParser  = require("body-parser");

const knexConfig  = require("./knexfile");
const knex        = require("knex")(knexConfig[ENV]);
const knexLogger  = require('knex-logger');
const usersRoutes = require("./routes/users");

const app = express();

// Log knex SQL queries to STDOUT as well
app.use(knexLogger(knex));

// Initialize an express app with some security defaults
app
  .use(https)
  .use(helmet());

app.use("/api/users", usersRoutes(knex));

// Application-specific routes
// Add your own routes here!
app.get('/example-path', function (req, res, next) {
  res.json({ messageFromNodeHttpServer: "Hello and welcome!",
             clientsconnected: wss.clients.size
           });
});

// Create the WebSockets server
const server = http.createServer(app);
const wss = new WebSocket({ server });

// Set up a callback that will run when a client connects to the server
// When a client connects they are assigned a socket, represented by
// the ws parameter in the callback.
wss.on('connection', (client) => {
  console.log('Client connected');
  //broadcast message to display number of users connected
  let message = {
    id: uuid.v4(),
    type: "onlineStatus",
    content: `${wss.clients.size} users connected`,
    room: client.room
  };
  wss.broadcast(message);

  client.on('message', (message) => {
  // Receive message, parse, then add unique id so React behaves correctly
  let incomingMessage = JSON.parse(message);
    switch(incomingMessage.type) {
        case "initialMsg":
          // set client's room on connection
          client.room = incomingMessage.room
        break;
        default:
          console.log('User ' + incomingMessage.username + " said " + incomingMessage.content );
          //generate random id and assign it to message as id
          incomingMessage.id = uuid.v4();
          wss.broadcast(incomingMessage);
          console.log(`Sent: ${incomingMessage}`);
      }
  });

  // Set up a callback for when a client closes the socket. This usually means they closed their browser.
  client.on('close', () => {
      let message = {
      id: uuid.v4(),
      type: "onlineStatus",
      content: `${wss.clients.size} users connected`
      // room: message.room
    };
    console.log(`Client disconnected: ${wss.clients.size} connections left`);
    wss.broadcast(message);
  });
});

//function to reverse url
function reverseUrl(str) {
  if (str){
    return "/chat/"+str.substring(6).split('+').reverse().join('+')
  };
};

// Broadcast - Goes through each client and sends message data
wss.broadcast = function(message) {
  wss.clients.forEach(function(client) {
    console.log(`client.room: ${client.room}`);
    console.log(`message.room: ${message.room}`);
    console.log(`message: ${message}`);
    // let clientReverseRoom = client.room;
    if(client.room === message.room || reverseUrl(client.room) === message.room ){
      // console.log(`reverseUrl(message.room): ${reverseUrl(message.room)}`);
      client.send(JSON.stringify(message));
      console.log(`reverseUrl(client.room): ${reverseUrl(client.room)}`);
    }
  });
};


// Serve static assets built by create-react-app
app.use(express.static('build'));

// If no explicit matches were found, serve index.html
app.get('*', function(req, res){
  res.sendFile(__dirname + '/build/index.html');
});

app
  .use(notfound)
  .use(errors);

function https(req, res, next) {
  if (process.env.NODE_ENV === 'production') {
    const proto = req.headers['x-forwarded-proto'];
    if (proto === 'https' || proto === undefined) {
      return next();
    }
    return res.redirect(301, `https://${req.get('Host')}${req.originalUrl}`);
  } else {
    return next();
  }
}

function notfound(req, res, next) {
  res.status(404).send('Not Found');
}

function errors(err, req, res, next) {
  console.log(err);
  res.status(500).send('something went wrong');
}

server.listen(PORT, () => console.log(`index is Listening on ${PORT}`));
