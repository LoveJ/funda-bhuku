"use strict";

const express = require('express');
const router  = express.Router();
let users = {};

module.exports = (knex) => {

  router.get("/", (req, res) => {
    knex
      .select("*")
      .from("users")
      .then((results) => {
        res.json(results);
        //assign results onto the usersObject object to make them accessible to the next post route
        users = results;
        console.log(results)
    });
  });

  return router;
}